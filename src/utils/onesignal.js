const enableNotification = (userId) => {
  OneSignal.sendTags({ user_id: userId })
}

const disableNotification = () => {
  OneSignal.deleteTag("user_id")
}

export { enableNotification, disableNotification }
