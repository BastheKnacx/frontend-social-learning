/** CONSTANTS */
import { registerStudent } from "../../../../constants/form"
import { IMG_LOGO, IMG_FACEBOOK, IMG_GOOGLE } from "../../../../constants/media"
import { rules } from "../../../../utils/validate"
import { genders } from "../../../../constants/selections"

/** FUNCTIONS */
import { signup, socialGoogleLogin, socialFacecbookLogin } from "../../../../services/auth"

/** COMPONENTS */
import InputGroup from "../../../../components/InputGroup.vue"
import SplitDateSelect from "../../../../components/SplitDateSelect.vue"
import RadioGroup from "../../../../components/RadioGroup.vue"
import BtnSpecial from "../../../../components/buttons/BtnSpecial.vue"
export default {
  data: () => ({
    imgLogo: IMG_LOGO,
    imgFacebook: IMG_FACEBOOK,
    imgGoogle: IMG_GOOGLE,
    student: registerStudent,
    rules: {
      required: rules.required,
      email: rules.email,
      phone: rules.phone,
      match: rules.match,
      password: rules.password,
      idCard: rules.idCard
    },
    genders: genders,
    layout: {
      half: "col-12 col-sm-4 col-md-4 col-xl-3",
      full: "col-12 col-sm-8 col-md-8 col-xl-6"
    },
    isValid: false,
    isLoading: false
  }),
  components: {
    InputGroup,
    SplitDateSelect,
    RadioGroup,
    BtnSpecial
  },
  methods: {
    socialGoogleLogin: socialGoogleLogin,
    socialFacecbookLogin: socialFacecbookLogin,
    async submit() {
      const isValid = this.$refs.registerForm.validate()
      if (isValid) {
        this.isLoading = true
        await signup(this.student, "student")
        this.isLoading = false
      }
    }
  }
}
