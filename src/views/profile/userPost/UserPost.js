import {axiosPost } from "../../../utils/api"

export default {
    data:() => ({
        
            search: null,
            token: localStorage.getItem("token"),
            userId: localStorage.getItem("userId"),
            img: localStorage.getItem("img"),
            id: this.$route.params.id,
            apiUrl: API_URL,
            feed: [],
            loading: true,
            imgIndex: null,
            arrImg: [],
            comments: [],
            reply: []
    }),
    created() {
        this.fetchFeed(this.id);
    },
    methods: {
        fetchFeed(id) {
            const data = { token: this.token, user_id: id };

            this.axios
                .post(`${API_URL}/api/user/post`, data, {
                    headers: header,
                })
                .then((result) => {
                    const { code, data: data, message } = result.data;
                    if (code == 411 || code == 412 || code == 413) {
                        this.autoLogout()
                    } else if (code == 301 || code == 302 || code == 305) {
                        alert(message)
                     } else if(code == 500){
                        //alert(message)
                    } else {
                        this.feed = data
                    }
                })
                .catch(() => {
                    alert('เกิดข้อผิดพลาด')
                })
        },
        editFeed(id, type) {
            if (type == 'post') {
                this.$router.push({
                    path: `/post/edit/${id}`
                })
            } 
            else {
                this.$router.push({
                    path: `/course/edit/${id}`
                })
            }
        },
        deleteFeed(id, type) {
            var url = 'api/course/delete'
            if (type == 'post') {
                var url = 'api/post/delete'
            }
            var body = { token: this.token, post_id: id };
            if (confirm('ต้องการลบโพสต์หรือไม่')) {

                this.axios
                    .post(`${API_URL}/${url}`, body, {
                        headers: header,
                    })
                    .then((res) => {
                        const { code, message } = res.data;
                        if (code == 411 || code == 412 || code == 413) {
                            this.autoLogout()
                        } else if (code == 301 || code == 302 || code == 305) {
                            alert(message)
                        } else if (code == 500) { 
                             //alert(message) 
                         } else {
                            this.fetchFeed(this.id)
                        }
                    })
                    .catch(() => {
                        alert('เกิดข้อผิดพลาด')
                    })
            }
        },
       autoLogout() {
            OneSignal.deleteTag("user_id");
            const token = { token: this.token };

            this.axios
                .post(`${API_URL}/api/logout`, token, {
                    headers: header,
                })
                .then((result) => {
                    const { code, data: data, message } = result.data;
                    if (code !== 200) {
                         localStorage.clear();
                        this.$router.push({
                            path: '/login'
                        });
                    } else {
                        localStorage.clear();
                        this.$router.push({
                            path: '/login'
                        });
                    }
                })
        },
        popupGallery(arr, index) {
            var images = []
            for (let i = 0; i < arr.length; i++) {
                images.push(`${this.apiUrl}/${arr[i].file.file}`)
            }
            this.arrImg = images
            this.imgIndex = index
        },
        sendComment(e, i, id) {
            const body = { token: this.token, post_id: id, comment: this.comments[i] }
            if (e.keyCode == 13 && this.comments[i] !== null) {
                this.axios
                    .post(`${API_URL}/api/comment/create`, body, {
                        headers: header,
                    })
                    .then((res) => {
                        const { code, data: data, message } = res.data;
                        if (code == 411 || code == 412 || code == 413) {
                            this.autoLogout()
                        } else if (code == 301 || code == 302 || code == 305) {
                            alert(message)
                        } else if (code == 500) { 
                             //alert(message) 
                         } else {
                            this.comments[i] = ''
                            this.fetchFeed(this.id)
                        }
                    })
                    .catch(() => {
                        alert('เกิดข้อผิดพลาด')
                    })

            }

        },
        sendReply(e, post, id, comment) {
            const body = { token: this.token, post_id: post, comment_id: id, comment: comment }
            if (e.keyCode == 13 && comment !== null) {
                this.axios
                    .post(`${API_URL}/api/subComment/create`, body, {
                        headers: header,
                    })
                    .then((res) => {
                        const { code, data: data, message } = res.data;
                        if (code == 411 || code == 412 || code == 413) {
                            this.autoLogout()
                        } else if (code == 301 || code == 302 || code == 305) {
                            alert(message)
                        } else if (code == 500) { 
                             //alert(message) 
                         } else {
                            this.fetchFeed(this.id)
                        }
                    })
                    .catch(() => {
                        alert('เกิดข้อผิดพลาด')
                    })
            }

        },
        actionPost(id, action) {
            const body = {
                token: this.token,
                post_id: id,
                user_id: this.userId,
                action: action
            }
            var url = "api/post/emoticon"
            if (action == 'favorite') {
                url = "api/post/favorite"
            } else {
                var url = "api/post/emoticon"
            }
            this.axios
                .post(`${API_URL}/${url}`, body, {
                    headers: header,
                })
                .then((result) => {
                    const { data: { code, message } } = result
                    if (code == 411 || code == 412 || code == 413) {
                        this.autoLogout()
                    } else if (code == 301 || code == 302 || code == 305) {
                        alert(message)
                     } else if(code == 500){
                        //alert(message)
                    } else {
                        this.fetchFeed(this.id)
                    }
                }).catch(() => {
                    alert('ระบบขัดข้อง กรุณาลองใหม่อีกครั้ง')
                })
        }
    },
};