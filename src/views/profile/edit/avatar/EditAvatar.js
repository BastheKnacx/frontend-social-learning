import { mapMutations, mapState } from "vuex"
import { MSG_VALIDATE_IMAGE, MSG_CONFIRM_CHANGE, MSG_UPDATE_SUCCESS } from "../../../../constants/messages"

import Avatar from "../../../../components/Avatar.vue"
import BoxUpload from "../../../../components/BoxUpload.vue"
import BtnSm from "../../../../components/buttons/BtnSm"
import { API_STUDENT_UPDATE, API_TEACHER_UPDATE } from "../../../../constants/endpoints"
import { axiosPost } from "../../../../utils/api"
import { confirmAlert, errorAlert, successAlert } from "../../../../utils/alert"

let formData = new FormData()
export default {
  data: () => ({
    imgProfile: [],
    imgProfileType: [],
    isStarter: false,
    isLoading: false
  }),
  components: {
    Avatar,
    BoxUpload,
    BtnSm
  },
  mounted() {
    this.imgProfile = this.user.img
    this.isStarter = true
  },
  computed: {
    ...mapState(["user"])
  },
  methods: {
    ...mapMutations({ setAuth: "SET_AUTH" }),
    async onchangeHandlerFile(image) {
      const imgExt = ["image/png", "image/jpg", "image/jpeg"]
      if (image.type && imgExt.includes(image.type) && image.size < 5000000) {
        formData = new FormData()
        formData.append("profile", image)
        this.imgProfile = URL.createObjectURL(image)
      } else {
        await errorAlert(MSG_VALIDATE_IMAGE)
      }
    },
    async submit() {
      if (this.imgProfile[0].includes("/uploads")) return

      const isConfirm = await confirmAlert(MSG_CONFIRM_CHANGE + "รูปโปรไฟล์หรือไม่?")
      if (isConfirm) {
        this.isLoading = true
        let endpoint = API_TEACHER_UPDATE
        if (this.user.type == "student") {
          endpoint = API_STUDENT_UPDATE
        }
        formData.append("token", this.user.token)
        formData.append("user_id", this.user.userId)
        formData.append("file_type", "profile")
        const { data, code, message } = await axiosPost(endpoint, formData)

        if (code == 200) {
          this.setAuth(data)
          await successAlert(MSG_UPDATE_SUCCESS)
        } else {
          formData.delete("token")
          formData.delete("user_id")
          formData.delete("file_type")
          await errorAlert(message)
        }
        this.isLoading = false
      }
    }
  }
}
