import { API_URL, header } from "@/constants/config"
export default {
  data() {
    return {
      msg: "",
      user: [],
      userId: localStorage.getItem("userId"),
      token: localStorage.getItem("token"),
      fullname: localStorage.getItem("fullname"),
      username: localStorage.getItem("username"),
      type: localStorage.getItem("type")
    }
  },
  created() {
    this.fetchUser()
  },
  methods: {
    fetchUser() {
      const body = { token: this.token, user_id: this.userId }
      this.axios
        .post(`${API_URL}/api/user/profile`, body, {
          headers: header
        })
        .then((result) => {
          const {
            data: { data },
            code,
            message
          } = result
          if (code == 411 || code == 412 || code == 413) {
            this.autoLogout()
          } else if (code == 301 || code == 302 || code == 305) {
            alert(message)
          } else if (code == 500) {
            //alert(message)
          } else {
            this.user = data
          }
        })
    }
  }
}
