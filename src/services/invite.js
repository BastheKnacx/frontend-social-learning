import router from "../router"
import { MSG_ADD_SUCCESS } from "../constants/messages"
import { errorAlert, successAlert } from "../utils/alert"
import { axiosPost } from "../utils/api"

const invite = async (group_id, user_id, invite_id) => {
    const { code, message } = await axiosPost(API_GROUP_INVITE, { token: router.state.user.userId, group_id, user_id, invite_id })
    if (code == 200) {
        await successAlert(MSG_ADD_SUCCESS)
    } else {
        await errorAlert(message)
    }
}

export { invite }