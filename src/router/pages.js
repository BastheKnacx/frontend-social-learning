/**
 * MAIN PAGE LIST
 */
import Feed from "../views/feed/Feed.vue"
import Profile from "../views/profile/Profile.vue"
import Notification from "../views/notification/Notification.vue"
import Saved from "../views/saved/Saved.vue"
import Message from "../views/message/Message.vue"
import Setting from "../views/setting/Setting.vue"
import Help from "../views/help/Help.vue"
import Login from "../views/auth/login/Login.vue"
import Register from "../views/auth/register/Register.vue"
import ResetPassword from "../views/auth/resetPassword/ResetPassword.vue"

/**
 * CHILDREN OF REGISTER PAGE
 */
import StudentRegister from "../views/auth/register/student/StudentRegister.vue"
import TeacherRegister from "../views/auth/register/teacher/TeacherRegister.vue"

/**
 * CHILDREN OF POST PAGE
 */
import ViewPost from "../views/post/view/ViewPost.vue"
import EditPost from "../views/post/edit/EditPost.vue"

/**
 * CHILDREN OF COURSE PAGE
 */
import AllCourse from "../views/course/AllCourse.vue"
import ViewCourse from "../views/course/view/ViewCourse.vue"
import ViewCourseByGroup from "../views/course/viewByGroup/ViewCourseByGroup.vue"
import CreateCourse from "../views/course/create/CreateCourse.vue"
import EditCourse from "../views/course/edit/EditCourse.vue"

/**
 * CHILDREN OF PROFILE PAGE
 */
import Timeline from "../views/profile/timeline/Timeline.vue"
import Liked from "../views/profile/liked/Liked.vue"
// import UserPost from "../views/profile/userPost/UserPost.vue"
import UserSaved from "../views/profile/saved/Saved.vue"

/**
 * CHILDREN OF EDIT PROFILE PAGE
 */
import EditProfile from "../views/EditProfile.vue"
import EditAvatar from "../views/profile/edit/avatar/EditAvatar.vue"
import EditCover from "../views/profile/edit/cover/EditCover.vue"
import EditInfo from "../views/profile/edit/information/EditInfo.vue"
import EditPassword from "../views/profile/edit/password/EditPassword.vue"

/**
 * CHILDREN OF GROUP
 */
import AllGroup from "../views/group/AllGroup.vue"
import MemberGroup from "../views/group/member/Member.vue"
import TimelineGroup from "../views/group/timeline/Timeline.vue"
import InviteGroup from "../views/group/invite/Invite.vue"

export {
  Feed,
  Profile,
  Notification,
  Saved,
  Message,
  Setting,
  Help,
  Login,
  Register,
  StudentRegister,
  TeacherRegister,
  ViewPost,
  EditPost,
  AllCourse,
  ViewCourse,
  ViewCourseByGroup,
  CreateCourse,
  EditCourse,
  Timeline,
  Liked,
  // UserPost,
  UserSaved,
  EditProfile,
  EditAvatar,
  EditCover,
  EditInfo,
  EditPassword,
  AllGroup,
  MemberGroup,
  TimelineGroup,
  InviteGroup,
  ResetPassword
}
